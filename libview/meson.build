libview_inc = include_directories('.')

include_subdir = join_paths(pps_include_subdir, 'libview')

headers = files(
  'pps-document-model.h',
  'pps-job-scheduler.h',
  'pps-jobs.h',
  'pps-print-operation.h',
  'pps-view-presentation.h',
  'pps-view.h',
)

install_headers(
  headers,
  subdir: include_subdir,
)

sources = files(
  'pps-annotation-window.c',
  'pps-color-contrast.c',
  'pps-debug.c',
  'pps-document-model.c',
  #'pps-form-field-accessible.c',
  #'pps-image-accessible.c',
  'pps-jobs.c',
  'pps-job-scheduler.c',
  #'pps-link-accessible.c',
  #'pps-page-accessible.c',
  'pps-page-cache.c',
  'pps-pixbuf-cache.c',
  'pps-print-operation.c',
  'pps-view.c',
  #'pps-view-accessible.c',
  'pps-view-cursor.c',
  'pps-view-presentation.c',
)

marshal = 'pps-view-marshal'

marshal_sources = gnome.genmarshal(
  marshal,
  sources: marshal + '.list',
  prefix: 'pps_view_marshal',
  internal: true,
  extra_args: '--prototypes',
)

enum_types = 'pps-view-type-builtins'
enum_header_prefix = '''
#if !defined (__PPS_PAPERS_VIEW_H_INSIDE__) && !defined (PAPERS_COMPILATION)
#error "Only <papers-view.h> can be included directly."
#endif

#include <papers-document.h>
'''

enum_sources = gnome.mkenums_simple(
  enum_types,
  sources: headers,
  header_prefix: enum_header_prefix,
  decorator: 'PPS_PUBLIC',
  install_header: true,
  install_dir: join_paths(pps_includedir, include_subdir),
)

deps = [
  gtk_unix_print_dep,
  gthread_dep,
  libppsdocument_dep,
  libaw_dep,
  m_dep,
]

cflags = [
  '-DG_LOG_DOMAIN="PapersView"',
  '-DPAPERS_COMPILATION'
]

sources += gnome.compile_resources(
  'pps-view-resources',
  'libview.gresource.xml',
  source_dir: data_dir,
  c_name: 'pps_view',
)


libppsview = shared_library(
  'ppsview-' + pps_api_version,
  version: pps_view_version,
  sources: sources + marshal_sources + enum_sources,
  include_directories: top_inc,
  dependencies: deps,
  c_args: cflags,
  install: true,
  gnu_symbol_visibility: 'hidden',
)

libppsview_dep = declare_dependency(
  sources: headers + [enum_sources[1]],
  include_directories: libview_inc,
  dependencies: deps,
  link_with: libppsview,
)

pkg.generate(
  libppsview,
  filebase: 'papers-view-' + pps_api_version,
  name: 'Papers View',
  description: 'Papers view library',
  subdirs: pps_include_subdir,
  requires: [
    'papers-document-@0@ = @1@'.format(pps_api_version, pps_version),
    gthread_dep,
  ],
  variables: 'exec_prefix=${prefix}',
)

install_headers(
  files('papers-view.h'),
  subdir: pps_include_subdir,
)

# GObject Introspection
if enable_introspection
  incs = [
    'Gdk-4.0',
    'GdkPixbuf-2.0',
    'Gio-2.0',
    'GLib-2.0',
    'GObject-2.0',
    'Gtk-4.0',
    libppsdocument_gir[0],
  ]

  libppsview_gir = gnome.generate_gir(
    [libppsview, libppsdocument],
    sources: sources + headers + [enum_sources[1]],
    includes: incs,
    nsversion: pps_api_version,
    namespace: 'PapersView',
    identifier_prefix: pps_code_prefix,
    symbol_prefix: pps_code_prefix.to_lower(),
    export_packages: 'papers-view-' + pps_api_version,
    extra_args: '-DPAPERS_COMPILATION',
    install: true,
  )
endif
